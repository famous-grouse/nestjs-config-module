import * as dotenv from "dotenv";
import { readFileSync } from "fs";

export class ConfigService {
  private readonly envConfig: Record<string, string>;

  constructor(filePath: string) {
    this.envConfig = dotenv.parse(readFileSync(filePath));
  }

  public get(key: string): string {
    const value = this.envConfig[key];
    if (value === undefined) {
      throw new Error(`Environment value "${key}" is undefined."`);
    }
    return value;
  }
}
